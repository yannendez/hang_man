#Jeu du pendu
**Bienvenue sur mon jeu du pendu**
Ce jeu nécessite une partie serveur et une partie client. les deux n'ont pas besoin d'être sur le même ordinateur.

Afin de jouer à ce jeu vous devez:
1. Compiler le programme en tapant `make` dans votre terminal
2. Exécuter en premier l'exécutable *serveur* avec la commande `./serveur`
3. Dans un autre terminal exécuter l'exécutable *client* avec la commande `./client`
4. Tout le reste s'effectuera du côté du client
5. Amusez vous !

(vous pouvez modifier les mot à trouver dans fichier.txt, il faudra remplacer un mot par un autre)
