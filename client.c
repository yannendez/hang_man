/******************************************************************************/
/*			Application: ...					*/
/******************************************************************************/
/*									      */
/*			 programme  CLIENT				      */
/*									      */
/******************************************************************************/
/*									      */
/*		Auteurs : ... 					*/
/*									      */
/******************************************************************************/


#include <stdio.h>
#include <curses.h> 		/* Primitives de gestion d'ecran */
#include <sys/signal.h>
#include <sys/wait.h>
#include<stdlib.h>

#include "fon.h"   		/* primitives de la boite a outils */

#define SERVICE_DEFAUT "1111"
#define SERVEUR_DEFAUT "127.0.0.1"

void client_appli (char *serveur, char *service);


/*****************************************************************************/
/*--------------- programme client -----------------------*/
/*
*** DEROULEMENT CÔTÉ CLIENT***
* Demande de connexion
* envoie du niveau de difficulté
* Envoie des caractères
*Fermeture de connexion
*/

char tampon_emission[100];
char tampon_reception[200];


int main(int argc, char *argv[])
{

	char *serveur= SERVEUR_DEFAUT; /* serveur par defaut */
	char *service= SERVICE_DEFAUT; /* numero de service par defaut (no de port) */


	/* Permet de passer un nombre de parametre variable a l'executable */
	switch(argc)
	{
 	case 1 :		/* arguments par defaut */
		  printf("serveur par defaut: %s\n",serveur);
		  printf("service par defaut: %s\n",service);
		  break;
  	case 2 :		/* serveur renseigne  */
		  serveur=argv[1];
		  printf("service par defaut: %s\n",service);
		  break;
  	case 3 :		/* serveur, service renseignes */
		  serveur=argv[1];
		  service=argv[2];
		  break;
    default:
		  printf("Usage:client serveur(nom ou @IP)  service (nom ou port) \n");
		  exit(1);
	}



	client_appli(serveur,service);
}

/*****************************************************************************/
void client_appli (char *serveur,char *service)

/* procedure correspondant au traitement du client de votre application */

{

		/* serveur est le nom (ou l'adresse IP) auquel le client va acceder */
		/* service le numero de port sur le serveur correspondant au  */
		/* service desire par le client */


	 //Etablissement de la connexion ===============================================

	 int id = h_socket(AF_INET,SOCK_STREAM); //initialisation de la socket
	 struct sockaddr_in* socket_client;
	 char myname[100 +1] ; /* 100 est une constante
	predefinie*/
	gethostname(myname, 100);
	printf("%s\n",myname );
	 struct sockaddr_in* socket_server;
	 adr_socket(service,serveur,SOCK_STREAM,&socket_server);
	 h_connect(id,socket_server);

// Fin d'établissement de la connexion ===========================================
//début du jeu
char buffer[24];
char saisie;
 int difficulte = 1;
 printf("choisissez un niveau de difficulté de 1 à 3\n");
 scanf("%d",&difficulte);
 if(difficulte > 3)
 	difficulte = 3;
	if(difficulte < 1)
		difficulte = 1;

	sprintf(buffer,"%d",difficulte);

	h_writes(id,buffer,24);

	h_reads(id,tampon_reception,40);
	printf("%s\n",tampon_reception);

	int vie,compteur;
	printf("\nsaisissez une lettre\n");
	scanf(" %c",&saisie);
	h_writes(id,&saisie,1);
	vie = 1;
	compteur = 1;

	while(vie > 0 && compteur > 0){
		h_reads(id,tampon_reception,15); // reception du mot
		printf("%s\n",tampon_reception);
		h_reads(id,tampon_reception,10); //reception du nombre de vies restantes
		vie = atoi(tampon_reception);


		h_reads(id,tampon_reception,10); // reception du nombre de lettres à trouver restantes
		compteur = atoi(tampon_reception);



		h_reads(id,tampon_reception,15); // reception du mot
		printf("Nombre de vies restantes : %d\n",vie);
		printf("Nombre de lettres à trouver restantes : %d\n", compteur);
		printf("%s\n",tampon_reception);

		if(vie < 1 || compteur < 1)
			break;
		printf("\nsaisissez une lettre\n");
		scanf(" %c",&saisie);
		h_writes(id,&saisie,1);
	}

	h_reads(id,tampon_reception,60);
	printf("%s\n",tampon_reception);
	h_shutdown(id,FIN_ECHANGES);
	h_close(id);
	return;
 }

/*****************************************************************************/
