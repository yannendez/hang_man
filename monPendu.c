#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <time.h>


char* choisiMot(int indice){
        FILE* fichierLecture = fopen("fichier.txt","r");
        if(fichierLecture != NULL){
            int compteurCol = 0;
            int compteurLigne = 0;
            int carActuel = 0;
            char* retour = malloc(15*sizeof(char));
            if(retour != NULL){
                while(carActuel != EOF){
                    carActuel = fgetc(fichierLecture);
                    if (compteurLigne == indice ){
                        while(carActuel != 10){
                            retour[compteurCol] = carActuel;
                            compteurCol++;
                            carActuel = fgetc(fichierLecture);
                        }
						retour[compteurCol] = 0;
                        compteurCol = 0;
                    }
                    if(carActuel == 10)
                        compteurLigne++;
                    }
            }
            fclose(fichierLecture);
            return retour;
        }
        fclose(fichierLecture);
        return NULL;
    }


bool checkLettre(char c, char* tab, int* p) // permet de vérifier si un lettre appartient à un mot ou non
{
	bool check=false;
	int compteur = 0;
	for(int i=0; tab[i] != 0; i++)
	{
		if(tab[i] == c) 
		{
			check = true;
			compteur++;
		}

	}
	printf("\nla lettre est présente %d fois dans le mots\n",compteur);
	*p = compteur;
	return check;
}

bool dejaSaisi(char c, char* tab)
{
	for(int i = 0; tab[i]!=0; i++)
	{
		if(c == tab[i]) {return true;}
	}
	return false;
}

void addChar(char c, char* tab)
{
	int size = strlen(tab);
	tab[size] = tab[size -1];
	tab[size] = c;

}

void afficherMot(char* mot, char* lettresSaisies){
	int tailleMot = strlen(mot);
	int tailleLettresSaisies = strlen(lettresSaisies);
	bool saisie = false;

	for(int i =0; i<tailleMot;i++){
		for(int j=0; j<tailleLettresSaisies;j++){
			if(mot[i] == lettresSaisies[j]){
				saisie = true;
			}
		}
		if(saisie == true){
			printf("%c",mot[i]);
		}
		else{
			printf("-");
		}
		saisie = false;
	}
	printf("\n");
}
int nbLigne(){
        FILE* fichier = fopen("fichier.txt","r");
        int compteur = 0;
        if(fichier){
            int carActuel = 0;
            while(carActuel != EOF){
                carActuel = fgetc(fichier);
                if(carActuel == 10)
                    compteur++;
            }
        }
        fclose(fichier);
        return compteur;
    }

int main()
{
	printf("bienvenue sur mon jeu du pendu !\n");


	bool check; //pour vérifier que la lettre appartient au mot
	int* pCompteur = (int*) malloc(sizeof(int)); // pour compter l'occurence d'une lettre dans un mot
	char saisi[24]=""; //pour faire une récap des lettres déjà utilisée
	bool dejaFait = false;
	int vie = 10; //nombre de vies

	char lettre; //lettre que l'on veut vérifier

	srand(time(NULL));
	int x = nbLigne(); // compte le nombre de mot ainsi pas besoin de modifier le programme pour rajouter des mots dans le fichier
	int n = rand() % x; // pour choisir un mot aléatoire dans la liste de fichier.txt
	char* mot = choisiMot(n);
	int compte = strlen(mot); //pour compter le nombre de lettre à trouver pour gagné

	while(compte > 0)
	{
		printf("\nsaisissez la lettre que vous voulez ici: \t");
		scanf(" %c",&lettre);

		check = checkLettre(lettre, mot, pCompteur);
		dejaFait = dejaSaisi(lettre,saisi);
		if(!dejaFait) {addChar(lettre,saisi);}
		if(check && !dejaFait) {compte -= *pCompteur;}
		if(!check && !dejaFait) {vie--;}

		printf("\nil vous reste %d chances\n",vie);
		printf("le mot ressemble à \t"); afficherMot(mot,saisi);
		printf("les lettres que vous avez saisies sont : %s\n",saisi);

		if(vie == 0) // Game over
		{
			printf("désolé vous avez perdu ... le mot était %s : \n",mot);
			return 0;
		}
	}

	printf("BRAVO ! tu as trouvé le mot : %s\n",mot);


}
