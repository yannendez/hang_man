/******************************************************************************/
/*			Application: ....			              */
/******************************************************************************/
/*									      */
/*			 programme  SERVEUR 				      */
/*									      */
/******************************************************************************/
/*									      */
/*		Auteurs :  ....						      */
/*		Date :  ....						      */
/*									      */
/******************************************************************************/

#include<stdio.h>
#include <curses.h>
#include <string.h>
#include <time.h>
#include<sys/signal.h>
#include<sys/wait.h>
#include<stdlib.h>

#include "fon.h"     		/* Primitives de la boite a outils */
#define TRUE 1
#define FALSE 0

#define SERVICE_DEFAUT "1111"

void serveur_appli (char *service);   /* programme serveur */


/******************************************************************************/
/*---------------- programme serveur ------------------------------*/

/*
*** DÉROULEMENT CÔTÉ SERVEUR***
* Attente de demande de connexion (tjr sur écoute)
* Envoie de la longueur du mot à deviner
* Envoie du nombre d'essaie restant et de la chaine de caractère du mot
*/

char* choisiMot(int indice){
    FILE* fichierLecture = fopen("fichier.txt","r");
    if(fichierLecture != NULL){
        int compteurCol = 0;
        int compteurLigne = 0;
        int carActuel = 0;
        char* retour = malloc(15*sizeof(char));
        if(retour != NULL){
            while(carActuel != EOF){
                carActuel = fgetc(fichierLecture);
                if (compteurLigne == indice ){
                    while(carActuel != 10){
                        retour[compteurCol] = carActuel;
                        compteurCol++;
                        carActuel = fgetc(fichierLecture);
                    }
				retour[compteurCol] = 0;
                    compteurCol = 0;
                }
                if(carActuel == 10)
                    compteurLigne++;
                }
        }
        fclose(fichierLecture);
        return retour;
    }
    fclose(fichierLecture);
    return NULL;
}

/*
*Retourne le nombre d'occurence de la lettre dans le mot.
*/

void addChar(char c, char* tab)
{
	int size = strlen(tab);
	tab[size] = tab[size -1];
	tab[size] = c;
}


int checkLettre(char c, char* tab)
{
	int compteur = 0;
	for(int i=0; tab[i] != 0; i++)
	{
		if(tab[i] == c)
		{
			compteur++;
		}

	}
	printf("\nla lettre est présente %d fois dans le mots\n",compteur);
	return compteur;
}

int dejaSaisi(char c, char* tab)
{
	for(int i = 0; tab[i]!=0; i++)
	{
		if(c == tab[i]) {return TRUE;}
	}
	addChar(c,tab);
	return FALSE;
}


void afficherMot(char* mot, char* lettresSaisies,int id_accept){
	int tailleMot = strlen(mot) -1;
	int tailleLettresSaisies = strlen(lettresSaisies) -1;
	int saisie = FALSE;
	char buffer[tailleMot-1];
	for(int i =0; i<tailleMot;i++){
		for(int j=0; j<=tailleLettresSaisies;j++){
			if(mot[i] == lettresSaisies[j]){
				saisie = TRUE;
			}
		}
		if(saisie == TRUE){
			addChar(mot[i],buffer);
		}
		else{
			addChar('-',buffer);
		}
		saisie = FALSE;
	}
	strcat(buffer,"\0\n");
	h_writes(id_accept,buffer,15);
}



char tampon_emission[150] = "";
char tampon_reception[200] = "";

int main(int argc,char *argv[])
{

	char *service= SERVICE_DEFAUT; /* numero de service par defaut */


	/* Permet de passer un nombre de parametre variable a l'executable */
	switch (argc)
 	{
   	case 1:
		  printf("defaut service = %s\n", service);
		  		  break;
 	case 2:
		  service=argv[1];
            break;

   	default :
		  printf("Usage:serveur service (nom ou port) \n");
		  exit(1);
 	}

	/* service est le service (ou numero de port) auquel sera affecte
	ce serveur*/

	//INITIALISATION DE LA connexion
	/*int id = h_socket(AF_INET,SOCK_STREAM); //initialisation de la socket
  struct sockaddr_in* socket_client;
  adr_socket(service,NULL,SOCK_STREAM,&socket_client);
	h_bind(id,socket_client);
	h_listen(id,4);


	int id_accept = h_accept(id,socket_client);*/

	serveur_appli(service);
}


/******************************************************************************/
void serveur_appli(char *service)

/* Procedure correspondant au traitemnt du serveur de votre application */

{

	/* A completer ... */
	/* service est le service (ou numero de port) auquel sera affecte
	ce serveur*/

	//INITIALISATION DE LA connexion
	int id = h_socket(AF_INET,SOCK_STREAM); //initialisation de la socket
	struct sockaddr_in* socket_client;
	adr_socket(service,NULL,SOCK_STREAM,&socket_client);
	h_bind(id,socket_client);

	h_listen(id,10);
	int id_accept = h_accept(id,socket_client);

	//récurperer un mot dans le fichier contenant les mots.
	h_reads(id_accept,tampon_reception,24);//récupétation de la difficulté permettant de définir le nombre de vies
	int vie = 0;
	int difficulte = atoi(tampon_reception);

	/*
	* il faut ensuite généré un mot qu'il faudra deviner
	*/

	srand(time(NULL));
	int rando = rand() % 50;
	char* mot = choisiMot(rando);
	//printf("le mot à deviner est %s",mot);
	int taille = strlen(mot) - 1;
	int compteur = taille; // pour compter le nombre de lettre restantes à trouver
	vie = taille / difficulte + 1;




	char buff[10];
	sprintf(buff,"%d",taille); //pour parser un entier en string
	strcpy(tampon_emission,"Longueur du mot : ");
	strcat(tampon_emission,buff);
	sprintf(buff,"%d",vie);
	strcat(tampon_emission,"\nNombre de vies : ");
	strcat(tampon_emission,buff);
	h_writes(id_accept,tampon_emission,40); //emission vers le client de la taille du mot


	char current;
	int nb_present,dejasaisi;
	char saisi[24]=" "; //pour faire une récap des lettres déjà utilisée
	do{
		h_reads(id_accept,&current,1); // récupération de la lettre émise pas le client
		printf("la lettre saisie est  %c",current);
		nb_present = checkLettre(current,mot);
		dejasaisi = dejaSaisi(current,saisi);

		afficherMot(mot,saisi,id_accept); // ATTENTION LA FONCTION UTILISE UN WRITE (15 octets)
		if(dejasaisi == FALSE){
			//addChar(current,saisi);
			nb_present > 0 ? compteur-=nb_present : vie --;
		}

		sprintf(buff,"%d",vie);
		h_writes(id_accept,buff,10); //envoie du nombre de vies restantes

		sprintf(buff,"%d",compteur);
		h_writes(id_accept,buff,10); //envoie du nombre de lettre à trouvé

		afficherMot(mot,saisi,id_accept); // ATTENTION LA FONCTION UTILISE UN WRITE (15 octets)
		printf("\nNombre de vies restantes : %d\n",vie);


	}while(compteur > 0 && vie > 0);


	if(vie < 1){
		strcpy(tampon_emission,"Vous avez perdu...\nle mot était ");
		strcat(tampon_emission,mot);

	}else if(compteur < 1){
		strcpy(tampon_emission,"Félicitations !\nvous avez trouver le mot ");
		strcat(tampon_emission,mot);
	}
	h_writes(id_accept,tampon_emission,60);
	h_shutdown(id_accept,FIN_ECHANGES);
	h_close(id_accept);
	return;

}

/******************************************************************************/
